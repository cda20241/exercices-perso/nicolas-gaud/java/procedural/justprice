package org.nicolasGAUD.game;

import jdk.jshell.execution.Util;
import org.json.simple.JSONObject;
import org.nicolasGAUD.utils.Utils;

import java.io.Console;
import java.util.Random;

public class Game {

    private static final int MIN = 1;
    private static final int MAX = 100;
    private static String EXIT = "exit";


    public JSONObject initGame(){
        String result = "";
        // Getting start time
        long starTime = System.currentTimeMillis();
        long endTime = 0;
        int counter = 0;
        String userName = "";
        // Printing menu, reading user input and checking input format while good format is not reached
        System.out.println("Bienvenue au Juste Prix ! \n");
        System.out.println("La partie va commencer après la saisie de votre nom. \n");
        while (userName.equalsIgnoreCase("") || userName.equalsIgnoreCase(EXIT) || userName.contains(":")){
            System.out.println("Votre nom (interdit : \"exit\", \":\", vide) : \n");
            userName = Utils.getScanner().nextLine();
        }
        // Clearing the console
        Utils.freeSpaceInConsole();
        // Getting a random number to find while not the same
        Random rand = new Random();
        int nbToFind = rand.nextInt(MAX - MIN + 1) + MIN;
        int userInput = -1;
        // Comparing user input and random number
        while (userInput != nbToFind){
            System.out.println("Veuillez entrer un nombre entre " + String.valueOf(MIN) + " et " + String.valueOf(MAX) + " : ");
            String strTmp = Utils.getScanner().nextLine();
            if (!strTmp.equalsIgnoreCase(""))
                userInput = Integer.parseInt(strTmp);
            else
                userInput = 0;
            counter++;
            Utils.freeSpaceInConsole();
            System.out.println("Votre saisie : " + userInput);
            if (userInput < nbToFind){
                System.out.println("C'est plus grand ! ");
            }
            else{
                System.out.println("C'est plus petit ! ");
            }
        }
        Utils.freeSpaceInConsole();
        System.out.println("Votre saisie : " + userInput);
        System.out.println(userName + ", vous avez gagné au bout de " + counter + " tentatives ! ");
        // Getting end time
        endTime = System.currentTimeMillis();
        // Getting elapsed time
        int elapsedTime = (int)endTime - (int)starTime;
        System.out.println("Votre score : " + elapsedTime);
        JSONObject newElement = new JSONObject();
        newElement.put("name", userName);
        newElement.put("score", elapsedTime);
        // Waiting for user to press "Enter"
        Utils.getScanner().nextLine();
        Utils.writeTxt(userName + ":" + elapsedTime);
        return newElement;
    }

}
