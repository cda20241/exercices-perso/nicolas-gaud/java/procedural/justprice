package org.nicolasGAUD.menu;

import org.nicolasGAUD.utils.Utils;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class Menu {

    private LinkedHashMap<String, String> CHOICES = new LinkedHashMap<>();
    private final String FROM_START = "0";
    private final String FROM_START_TO_RESULTS = "1";
    public LinkedHashMap<String, String> getChoices() {
        return CHOICES;
    }

    /**
     * This function builds and prints the menus depending on the parameter "whereFrom" indicating from which menu
     * we are coming.
     * @param whereFrom String
     * @return String
     */
    public String getMenu(String whereFrom, boolean isResearch){
        String userChoice = "";
        String message1 = "";
        String message2 = "";
        boolean isGoodInput = false;
        CHOICES.clear();
        while (!isGoodInput){
            // Building the menus
            if (whereFrom.equalsIgnoreCase(FROM_START)) {
                CHOICES.put("Menu de résulats : entrez 1", "1");
                CHOICES.put("Menu de partie : entrez 2", "2");
                CHOICES.put("Sortir : entrez \"exit\"", "exit");
            } else if (whereFrom.equalsIgnoreCase(FROM_START_TO_RESULTS)) {
                CHOICES.put("Retourner au menu : entrez \"menu\"", "menu");
                CHOICES.put("Sortir : entrez \"exit\"", "exit");
            }
            if (!whereFrom.equalsIgnoreCase(FROM_START_TO_RESULTS)){
                message1 = "Où voulez-vous aller ?";
                message2 = "Votre choix : ";
            }
            else{
                message1 = "Que voulez-vous faire ?";
                message2 = "Votre recherche (ou \"menu\" ou \"exit\") : ";
            }

            // Prints the menus
            System.out.println(message1);
            Iterator iterator = Utils.getIterator(CHOICES);
            while (iterator.hasNext()) {
                Map.Entry<String, String> element = (Map.Entry<String, String>) iterator.next();
                System.out.println(element.getKey());
            }
            System.out.println(message2);

            // Making input controls only if it's not a research
            if (!isResearch){
                // Read user input
                userChoice = Utils.getScanner().nextLine();
                if (CHOICES.containsValue(userChoice)){
                    isGoodInput = true;
                }
                else{
                    System.out.println("La saisie est incorrecte.\n");
                }
            }
            else{
                isGoodInput = true;
            }

        }
        return userChoice;
    }
}
