package org.nicolasGAUD.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.*;

public class Utils {

    public static final int NULL_SUBSET = -1;

    /**
    *This function creates and returns a Scanner to read inputs
    */
    public static Scanner getScanner() {
        return new Scanner(System.in);
    }

    /**
     * This function prints keys and values from a HashMap given in parameter.
     * @param hashMap HashMap<String, String>
     */
    public static void printList(HashMap<String, String> hashMap, int subSet){
        //Map sortedMap = new TreeMap(hashMap);
        ArrayList<String> arrayTmp = new ArrayList<>();

        for (Object key : hashMap.keySet()) {
            Object value = hashMap.get(key);
            arrayTmp.add("Nom : " + key.toString() + ", Score : " + value.toString());
        }
        int beginning = 0;
        if (subSet != NULL_SUBSET && subSet < arrayTmp.size()){
            beginning = arrayTmp.size() - subSet;
        }
        for (int i = beginning ; i < arrayTmp.size() ; i++){
            System.out.println(arrayTmp.get(i));
        }
        System.out.println();
    }

    /**
     * This function searches in a list if a string given in parameter is present and builds a new sorted map with
     * keys and values that matches.
     * @param hashMap HashMap<String, String>
     * @param strToCompare String
     */
    public static void searchInList(HashMap<String, String> hashMap, String strToCompare){
        HashMap<String, String> tmpMap = new HashMap<>();
        for (Object key : hashMap.keySet()) {
            Object value = hashMap.get(key);
            if (key.toString().toLowerCase(Locale.ROOT).contains(strToCompare.toLowerCase())){
                tmpMap.put(key.toString(), value.toString());
            }
        }
        printList(tmpMap,NULL_SUBSET);
    }

    /**
     * This function reads a Json file from a path given in parameter, then builds a HashMap with keys and values
     * from it
     * @param path String
     * @return HashMap<String, String>
     */
    public static HashMap<String, String> readJson(String path) {
        // Init objects
        HashMap<String, String> results = new HashMap<>();
        JSONObject jsonObject = new JSONObject();
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = new JSONArray();
        // Reading the Json file
        try{
            // Reading the json file
            Object obj = jsonParser.parse(new FileReader(path));
            // Convertir object to JSONArray
            jsonArray = (JSONArray) obj;
        }
        catch (IOException e){
            e.printStackTrace();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        try {
            // Not knowing any keys or values, transforms into an array, browses it and then puts it into a HashMap
        for (Object item : jsonArray) {
            jsonObject = (JSONObject) item;
            String name = (String) jsonObject.get("name");
            String score = (String) jsonObject.get("score");
            results.put(name, score);
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }

    /**
     * This function reads a property file and returns a property defined by its key
     * @param key string
     * @return a string with the property wanted
     */
    public static String getProperty(String key) {
        Properties prop = new Properties();
        InputStream input = null; // Initialisez la variable en dehors du try pour qu'elle soit accessible dans le bloc finally.

        try {
            // Gets an inputstream and read the properties
            input = new FileInputStream("./src/main/resources/config.properties");
            prop.load(input);
            return prop.getProperty(key);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } finally {
            // Assurez-vous que le flux est fermé, qu'une exception soit levée ou non.
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * this function returns the first element (key, value) of a Map
     * @param map Map to search in
     * @return the first element of the map
     */
    public static Map.Entry<String, String> getFirstLinkedMapElt(Map<String, String> map) {
        // Get first element
        Iterator iterator = getIterator(map);
        return (Map.Entry<String, String>) iterator.next();
    }

    /**
     * this function returns the last element (key, value) of a Map
     * @param map Map to search in
     * @return the last element of the map
     */
    public static Map.Entry<String, String> getLastLinkedMapElt(Map<String, String> map) {
        Iterator iterator = getIterator(map);
        // Get last element
        Map.Entry<String, String> lastElement = null;
        while (iterator.hasNext()) {
            lastElement = (Map.Entry<String, String>) iterator.next();
        }
        return lastElement;
    }

    /**
     * this function returns an iterator on the Map given in parameter
     * @param map Map
     * @return an Iterator
     */
    public static Iterator getIterator(Map<String, String> map){
        return map.entrySet().iterator();
    }

    /**
     * This function prints 100 blank lines in order to clear the console !
     */
    public static void freeSpaceInConsole(){
        for (int i = 0 ; i < 100 ; i++)
            System.out.println("\n");
    }

    /**
     * This function write a JSONObject into an existing Json file
     * @param result JSONObject
     */
    public static void writeJson(JSONObject result){
        JSONParser parser = new JSONParser();
        JSONArray jsonArray = new JSONArray();

        // Reading the contents of the jsonFile if exists and stores it into a JSONArray
        try (FileReader fileReader = new FileReader(Utils.getProperty("jsonPath"))) {
                Object obj = parser.parse(fileReader);
                jsonArray = (JSONArray) obj;
        } catch (IOException | ParseException e) {
            jsonArray = new JSONArray();
        }

        // Adding  result to the JSONArray
        jsonArray.add(result);

        // Writing the JSONArray into the Json file.
        try (FileWriter fileWriter = new FileWriter(Utils.getProperty("jsonPath"))) {
            fileWriter.write(jsonArray.toJSONString());
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * This function writes a string given in parameter into an existing txt file
     * @param result String
     */
    public static void writeTxt(String result){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(Utils.getProperty("txtPath"), true))) {
            writer.newLine();
            writer.write(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
