package org.nicolasGAUD;

import org.json.simple.JSONObject;
import org.nicolasGAUD.game.Game;
import org.nicolasGAUD.menu.Menu;
import org.nicolasGAUD.utils.Utils;

import java.util.HashMap;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {

        final String START_MENU = "0";
        final String RESULT_MENU = "1";
        final String GAME_MENU = "2";
        final String SEARCH_MENU = "10";
        final String SEARCH = "11";
        final String RETURN = "menu";
        final String EXIT = "exit";
        final int SUBSET = 10;
        final int NULL_SUBSET = -1;
        HashMap<String, String> results = Utils.readJson(Utils.getProperty("jsonPath"));

        Menu menu = new Menu();
        String choice = START_MENU;
        while (!choice.equalsIgnoreCase(EXIT)){
            if (choice.equalsIgnoreCase("")){
                choice = SEARCH;
            }
            switch (choice.toLowerCase(Locale.ROOT)){
                case EXIT:
                    System.exit(0);
                    break;
                case START_MENU:
                    Utils.printList(results,SUBSET);
                    choice = menu.getMenu(START_MENU,false);
                    break;
                case SEARCH_MENU:
                    choice = menu.getMenu(SEARCH_MENU, true);
                    break;
                case SEARCH:
                    while (!choice.equalsIgnoreCase(RETURN) && !choice.equalsIgnoreCase(EXIT)){
                        String strToSearch =  Utils.getScanner().nextLine();
                        // Disable the search in case of input "menu" or "exit"
                        if (!strToSearch.equalsIgnoreCase(RETURN) && !strToSearch.equalsIgnoreCase(EXIT)){
                            Utils.searchInList(results, strToSearch);
                            choice = menu.getMenu(RESULT_MENU, true);
                        }
                        else if (strToSearch.equalsIgnoreCase(EXIT)){
                            choice = EXIT;
                        }
                        else if (strToSearch.equalsIgnoreCase(RETURN)){
                            choice = RETURN;
                        }
                    }
                    break;
                case RETURN:
                    choice = menu.getMenu(START_MENU, false);
                    break;
                case RESULT_MENU:
                    Utils.printList(results,NULL_SUBSET);
                    choice = menu.getMenu(RESULT_MENU, true);
                    break;
                case GAME_MENU:
                    Game game = new Game();
                    JSONObject result = game.initGame();
                    Utils.writeJson(result);
                    choice = menu.getMenu(START_MENU, false);
                    break;
            }
        }

    }
}
